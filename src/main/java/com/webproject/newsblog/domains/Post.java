package com.webproject.newsblog.domains;

import com.webproject.newsblog.security.User;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "post")
public class Post  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(updatable = false, nullable = false)
    private long id;
    @NotBlank(message = "title of the post can't be empty")
    private String title;
    private Date dateCreated;
    @ManyToOne
    private User creator;
    @NotBlank(message = "body of the post can't be empty")
    @Lob
    private String body;
    private int views;
    private float averageRating;
//    @OneToMany
//    private List<Review> reviews;

//    public List<Review> getReviews() {
//        return reviews;
//    }
//
//    public void setReviews(List<Review> reviews) {
//        this.reviews = reviews;
//    }
//
//    public void setReviews(Review review){
//        reviews.add(review);
//    }

}
