package com.webproject.newsblog.domains;

import com.webproject.newsblog.security.User;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Data
@Entity
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    private User rater;

//    @Size(min = 1, max = 5, message = "The range of rating value is between 1 and 5")
    private int ratingValue;

    @ManyToOne
    private Post post;

    @Override
    public String toString() {
        return " Rating " + id + ":" + " \n Rater : " + rater.getUsername() + " \n Post : " + post.getTitle();
    }
}
