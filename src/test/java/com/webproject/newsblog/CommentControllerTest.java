package com.webproject.newsblog;


import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.webproject.newsblog.controllers.CommentController;
import com.webproject.newsblog.controllers.HomeController;
import com.webproject.newsblog.domains.Comment;
import com.webproject.newsblog.services.*;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

@RunWith(SpringRunner.class)
@WebMvcTest(CommentController.class)
public class CommentControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CommentService commentServiceMock;
    @MockBean
    private UserService userServiceMock;
    @MockBean
    private PostService postServiceMock;


    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testMakeAComment() throws Exception{
        Comment comment = new Comment();
        comment.setId(1);
        comment.setCreator(userServiceMock.findUserByUsername("nw"));
        comment.setPost(postServiceMock.findById(1));
        comment.setBody("test comment");
        comment.setDateCreated(new Date());
        commentServiceMock.addComment(comment);
    }
    @Test
    public void testDeleteComment(){
        Comment comment = commentServiceMock.commentById(1);
        commentServiceMock.deleteComment(comment);
    }
}
