package com.webproject.newsblog;

import com.webproject.newsblog.controllers.RatingController;
import com.webproject.newsblog.domains.Reply;
import com.webproject.newsblog.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@RunWith(SpringRunner.class)
@WebMvcTest(RatingController.class)
public class ReplyControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ReplyService replyServiceMock;
    @MockBean
    private UserService userServiceMock;
    @MockBean
    private CommentService commentServiceMock;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetAllReply() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/reply/{id}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("reply"))
                .andExpect(MockMvcResultMatchers.view().name("reply"))
                .andDo(print());
    }

    @Test
    public void testAddReply(){
        Reply reply = new Reply();
        reply.setId(1);
        reply.setDateCreated(new Date());
        reply.setComment(commentServiceMock.commentById(1));
        reply.setCreator(userServiceMock.findUserByUsername("nw"));
        reply.setBody("reply to test comment");
        replyServiceMock.addReply(reply);


    }

}
