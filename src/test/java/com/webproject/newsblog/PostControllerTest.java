package com.webproject.newsblog;


import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import com.webproject.newsblog.controllers.CommentController;
import com.webproject.newsblog.controllers.HomeController;
import com.webproject.newsblog.controllers.PostController;
import com.webproject.newsblog.domains.Post;
import com.webproject.newsblog.security.User;
import com.webproject.newsblog.services.*;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

@RunWith(SpringRunner.class)
@WebMvcTest(PostController.class)
public class PostControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PostService postServiceMock;
    @MockBean
    private CommentService commentServiceMock;
    @MockBean
    private UserService userServiceMock;
    @MockBean
    private RoleService roleServiceMock;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testPostHome() throws Exception{
//        assertThat(this.commentServiceMock).isNotNull();
        mockMvc.perform(MockMvcRequestBuilders.get("/posts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("index"))
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andDo(print());
    }
    @Test
    public void testIndex() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("index"))
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andDo(print());
    }

    @Test
    public void testSinglePost() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/post/{id}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("single_post"))
                .andExpect(MockMvcResultMatchers.view().name("single_post"))
                .andDo(print());
    }
    @Test
    public void testSpu() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/spu/{id}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("single_postU"))
                .andExpect(MockMvcResultMatchers.view().name("single_postU"))
                .andDo(print());
    }

    @Test
    public void testPostStory() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/postStory"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("post_story"))
                .andExpect(MockMvcResultMatchers.view().name("post_story"))
                .andDo(print());
    }
    @Test
    public void testPBC() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/postByCreator/{username}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("postsByCreator"))
                .andExpect(MockMvcResultMatchers.view().name("postsByCreator"))
                .andDo(print());
    }
    @Test
    public void testPbcu() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/pbc/{username}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("pbcu"))
                .andExpect(MockMvcResultMatchers.view().name("pbcu"))
                .andDo(print());
    }

    @Test
    public void testEditPost(){
        Post post = postServiceMock.findById(3);
        post.setTitle("testing edit post");
        post.setBody("edit post tested");
        postServiceMock.update(post);
    }

    @Test
    public void testAddPost(){
        Post post = new Post();
        post.setId(1);
        post.setTitle("test");
        post.setDateCreated(new Date());
        post.setBody("test post");
        post.setCreator(userServiceMock.findUserByUsername("nw"));
        post.setViews(0);
        post.setAverageRating(0);
        postServiceMock.addPost(post);

    }

    @Test
    public void testDeletePost(){
        Post post = postServiceMock.findById(1);
        postServiceMock.deletePost(post);
    }
}
