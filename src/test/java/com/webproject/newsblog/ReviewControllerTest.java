package com.webproject.newsblog;

import com.webproject.newsblog.controllers.PostController;
import com.webproject.newsblog.controllers.ReviewController;
import com.webproject.newsblog.domains.Review;
import com.webproject.newsblog.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;



@RunWith(SpringRunner.class)
@WebMvcTest(ReviewController.class)
public class ReviewControllerTest {

    @MockBean
    private ReviewService reviewServicMock;
    @MockBean
    private  PostService postServiceMock;
    @MockBean
    private  UserService userServiceMpck;
    @MockBean
    private  RatingService ratingServiceMock;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }



    @Test
    public void testGetReview() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/review/{id}/{message}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType("text/html;charset=UTF-8"))
                .andExpect(view().name("review"))
                .andExpect(MockMvcResultMatchers.view().name("review"))
                .andDo(print());
    }

    @Test
    public void testReviewPost(){
        Review review = new Review();
        review.setId(1);
        review.setReviewer(userServiceMpck.findUserByUsername("nw"));
        review.setReviewBody("test review");
        review.setReviewedPost(postServiceMock.findById(1));
        reviewServicMock.addReview(review);
    }
}
