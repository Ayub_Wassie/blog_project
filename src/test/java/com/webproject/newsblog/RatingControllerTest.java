package com.webproject.newsblog;


import com.webproject.newsblog.controllers.RatingController;
import com.webproject.newsblog.domains.Rating;
import com.webproject.newsblog.services.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@WebMvcTest(RatingController.class)
public class RatingControllerTest {

    @MockBean
    private RatingService ratingServiceMock;
    @MockBean
    private UserService userServiceMock;
    @MockBean
    private PostService postServiceMock;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testRate() {
        Rating rating = new Rating();
        rating.setId(1);
        rating.setPost(postServiceMock.findById(1));
        rating.setRater(userServiceMock.findUserByUsername("nw"));
        rating.setRatingValue(4);
        ratingServiceMock.addRating(rating);
    }

}
